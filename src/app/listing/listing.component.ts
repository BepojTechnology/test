import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  constructor() { }
  
  listing = [
	{ image: 'assets/Tile_Image.jpg',
	  titleImage: 'Title on image',
	  title : 'Listing title', 
	  weight: '1000 lbs, offset', 
	  des: 'Some quick example text to build on the card title and make up the bulk of the cards content.'
	},
	{ image: 'assets/Tile_Image.jpg',
	  titleImage: 'Title on image 1',
	  title : 'Listing title', 
	  weight: '2000 lbs, offset', 
	  des: 'Some quick example text to build on the card title and make up the bulk of the cards content.'
	},
	{ image: 'assets/Tile_Image.jpg',
	  titleImage: 'Title on image',
	  title : 'Listing title 2', 
	  weight: '5000 lbs, offset', 
	  des: 'Some quick example text to build on the card title and make up the bulk of the cards content.'
	},
	{ image: 'assets/Tile_Image.jpg',
	  titleImage: 'Title on image 3',
	  title : 'Listing title', 
	  weight: '5200 lbs, offset', 
	  des: 'Some quick example text to build on the card title and make up the bulk of the cards content.'
	}
  
  ]

  ngOnInit() {
  }

}
